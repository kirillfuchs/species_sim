from __future__ import annotations
import yaml
import pprint
from typing import List
from cerberus import Validator
import random
import sys
from models import Habitat, Species, Animal
from utils import ConfigReader, ConfigValidator
from constants import Gender, CauseOfDeath


class Scorer:
    def score(self, simulations: List[Simulation]):
        records = []
        species_name = simulations[0].species.name
        hab_name = simulations[0].habitat.name
        for sim in simulations:
            records.append(self._get_final_score(sim))

        pprint.pprint({
            species_name: {
                hab_name: self._get_final_for_all_sims(records)
            }
        })

    def _get_final_score(self, sim: Simulation):
        population = [record['population'] for record in sim.record]

        return {
            'avg_pop': sum(population) / sim.simulation_length,
            'max_pop': max(population),
            'total_pop': sim.species.current_population_total() + sim.species.total_deaths(),
            'total_deaths': sim.species.total_deaths(),
            'causes_of_deaths': {
                CauseOfDeath.STARVATION: sim.species.count_deaths_by_cause(CauseOfDeath.STARVATION),
                CauseOfDeath.THIRST: sim.species.count_deaths_by_cause(CauseOfDeath.THIRST),
                CauseOfDeath.AGE: sim.species.count_deaths_by_cause(CauseOfDeath.AGE),
                CauseOfDeath.COLD_WEATHER: sim.species.count_deaths_by_cause(CauseOfDeath.COLD_WEATHER),
                CauseOfDeath.HOT_WEATHER: sim.species.count_deaths_by_cause(CauseOfDeath.HOT_WEATHER),
            }
        }

    def _get_final_for_all_sims(self, records: list):
        iterations = len(records)
        avg_pops = []
        max_pops = []
        total_pop = 0
        total_deaths = 0
        deaths_by_cause = {
            CauseOfDeath.STARVATION: 0,
            CauseOfDeath.THIRST: 0,
            CauseOfDeath.AGE: 0,
            CauseOfDeath.COLD_WEATHER: 0,
            CauseOfDeath.HOT_WEATHER: 0,
        }

        for record in records:
            avg_pops.append(record['avg_pop'])
            max_pops.append(record['max_pop'])
            total_pop += record['total_pop']
            total_deaths += record['total_deaths']
            deaths_by_cause[CauseOfDeath.STARVATION] += record['causes_of_deaths'][CauseOfDeath.STARVATION]
            deaths_by_cause[CauseOfDeath.THIRST] += record['causes_of_deaths'][CauseOfDeath.THIRST]
            deaths_by_cause[CauseOfDeath.AGE] += record['causes_of_deaths'][CauseOfDeath.AGE]
            deaths_by_cause[CauseOfDeath.COLD_WEATHER] += record['causes_of_deaths'][CauseOfDeath.COLD_WEATHER]
            deaths_by_cause[CauseOfDeath.HOT_WEATHER] += record['causes_of_deaths'][CauseOfDeath.HOT_WEATHER]

        return {
            'avg_population': int(sum(avg_pops) // iterations),
            'max_population': max(max_pops),
            'mortality_rate': total_deaths / total_pop * 100,
            'causes_of_death': {
                CauseOfDeath.STARVATION: deaths_by_cause[CauseOfDeath.STARVATION] / total_deaths * 100,
                CauseOfDeath.THIRST: deaths_by_cause[CauseOfDeath.THIRST] / total_deaths * 100,
                CauseOfDeath.AGE: deaths_by_cause[CauseOfDeath.AGE] / total_deaths * 100,
                CauseOfDeath.COLD_WEATHER: deaths_by_cause[CauseOfDeath.COLD_WEATHER] / total_deaths * 100,
                CauseOfDeath.HOT_WEATHER: deaths_by_cause[CauseOfDeath.HOT_WEATHER] / total_deaths * 100,
            }
        }


class Simulation:
    def __init__(self, years: int, habitat: Habitat, species: Species):
        self.habitat = habitat
        self.species = species
        self.simulation_length = years * 12  # We increment time in months.

        self.record = []

    def run(self):
        for time in range(self.simulation_length):
            # Record beginning of month setup.
            record = {
                'turn': time,
                'month': self.habitat.month,
                'temp': self.habitat.temperature
            }

            # Start month, generate food/water, and count population after all births.
            self.habitat.generate_food().generate_water()
            record['population'] = self.species.gestate().current_population_total()

            # Now feed everyone, start breeding.
            self.species.feed().breed()

            # At end of month we do a general living check, and increment the month.
            self.species.habitate()
            self.habitat.increment_month()

            # Record any deaths
            record['deaths'] = {
                CauseOfDeath.STARVATION: len(self.species.deaths[CauseOfDeath.STARVATION]),
                CauseOfDeath.THIRST: len(self.species.deaths[CauseOfDeath.THIRST]),
                CauseOfDeath.AGE: len(self.species.deaths[CauseOfDeath.AGE]),
                CauseOfDeath.COLD_WEATHER: len(self.species.deaths[CauseOfDeath.COLD_WEATHER]),
                CauseOfDeath.HOT_WEATHER: len(self.species.deaths[CauseOfDeath.HOT_WEATHER]),
            }

            self.record.append(record)

        return self


class SimulationFactory:
    def create(self, years: int, habitat_data: dict, species_data: dict) -> Simulation:
        habitat = self.start_habitat(habitat_data)
        species = self.start_species(species_data)

        male = Animal(species, Gender.MALE, habitat)
        female = Animal(species, Gender.FEMALE, habitat)
        species.add_animal(male).add_animal(female)

        return Simulation(years, habitat, species)

    def start_habitat(self, hab_data: dict) -> Habitat:
        climate = [
            hab_data['average_temperature']['winter'],
            hab_data['average_temperature']['winter'],
            hab_data['average_temperature']['spring'],
            hab_data['average_temperature']['spring'],
            hab_data['average_temperature']['spring'],
            hab_data['average_temperature']['summer'],
            hab_data['average_temperature']['summer'],
            hab_data['average_temperature']['summer'],
            hab_data['average_temperature']['fall'],
            hab_data['average_temperature']['fall'],
            hab_data['average_temperature']['fall'],
            hab_data['average_temperature']['winter'],
        ]

        start_month = random.randint(0, 11)

        return Habitat(hab_data['name'], hab_data['monthly_food'], hab_data['monthly_water'], climate, start_month)

    def start_species(self, spe_data: dict) -> Species:
        attr = spe_data.get('attributes')

        return Species(
            spe_data.get('name'),
            attr.get('monthly_food_consumption'),
            attr.get('monthly_water_consumption'),
            attr.get('life_span') * 12,
            attr.get('minimum_breeding_age') * 12,
            attr.get('maximum_breeding_age') * 12,
            attr.get('gestation_period'),
            attr.get('minimum_temperature'),
            attr.get('maximum_temperature'),
            attr.get('max_survival_time_above_temp', 1),  # Set default since it won't show up in object.
            attr.get('max_survival_time_below_temp', 1),  # Set default since it won't show up in object.
            attr.get('max_survival_time_starvation', 3),  # Set default since it won't show up in object.
            attr.get('max_survival_time_dehydration', 2),  # Set default since it won't show up in object.
        )


class Runner:
    def run(self, config_file: str):
        data = ConfigReader(yaml, config_file, ConfigValidator(Validator)).read()
        years = data['years']
        iterations = data['iterations']

        for species_data in data['species']:
            for habitat_data in data['habitats']:

                sims = []
                for iteration in range(iterations):
                    sims.append(SimulationFactory().create(years, habitat_data, species_data).run())

                Scorer().score(sims)


if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] == '-h':
        print("Usage: \n  simulator.py <config_file>")
    else:
        try:
            Runner().run(sys.argv[1])
        except FileNotFoundError as err:
            pprint.pprint(str(err))
