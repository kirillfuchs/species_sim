from __future__ import annotations
from constants import Gender, CauseOfDeath
import random


class Habitat:
    def __init__(self, name: str, monthly_food_regen: int, monthly_water_regen: int, climate: list,
                 starting_month: int):
        self._monthly_food_regen = monthly_food_regen
        self._monthly_water_regen = monthly_water_regen
        self._climate = climate

        self.name = name
        self.month = starting_month
        self.food_supply = 0
        self.water_supply = 0
        self.temperature = self._generate_temp(self.month)

    def increment_month(self) -> Habitat:
        self.month = (self.month + 1) % 12  # Increment month by 1 restarting back to 1 after 12.
        self.temperature = self._generate_temp(self.month)
        return self

    def generate_water(self) -> Habitat:
        self.water_supply += self._monthly_water_regen
        return self

    def generate_food(self) -> Habitat:
        self.food_supply += self._monthly_food_regen
        return self

    def remove_food(self, amount: int) -> Habitat:
        self.food_supply -= amount
        return self

    def remove_water(self, amount: int) -> Habitat:
        self.water_supply -= amount
        return self

    def has_enough_food(self, food_req: int) -> bool:
        return self.food_supply >= food_req

    def has_enough_water(self, water_req: int) -> bool:
        return self.water_supply >= water_req

    def _generate_temp(self, month: int) -> int:
        return self._climate[month] - random.choices(
            (random.randrange(-5, 5), random.randrange(-15, -6), random.randrange(6, 15)), (99.5, 0.25, 0.25))[0]


# Simple species - does not take the welfare of its society into consideration
# Societal species - Makes decisions based on whats best for the society
# Why make the species responsible for the actions?
# Because this allows us to extend the species to be deterministic by different sets of rules.
class Species:
    def __init__(self, name: str, monthly_food_consumption: int, monthly_water_consumption: int, life_span: int,
                 minimum_breeding_age: int, maximum_breeding_age: int, gestation_period: int,
                 minimum_temperature: int,
                 maximum_temperature: int, max_survival_time_above_temp: int, max_survival_time_below_temp: int,
                 max_survival_time_starvation: int, max_survival_time_dehydration: int):
        self.name = name
        self.monthly_food_consumption = monthly_food_consumption
        self.monthly_water_consumption = monthly_water_consumption
        self.life_span = life_span
        self.minimum_breeding_age = minimum_breeding_age
        self.maximum_breeding_age = maximum_breeding_age
        self.gestation_period = gestation_period
        self.minimum_temperature = minimum_temperature
        self.maximum_temperature = maximum_temperature
        self.max_survival_time_above_temp = max_survival_time_above_temp
        self.max_survival_time_below_temp = max_survival_time_below_temp
        self.max_survival_time_starvation = max_survival_time_starvation
        self.max_survival_time_dehydration = max_survival_time_dehydration

        self.animals = {
            Gender.MALE: [],
            Gender.FEMALE: []
        }

        self.deaths = {
            CauseOfDeath.STARVATION: [],
            CauseOfDeath.THIRST: [],
            CauseOfDeath.AGE: [],
            CauseOfDeath.COLD_WEATHER: [],
            CauseOfDeath.HOT_WEATHER: [],
        }

    def add_animal(self, animal: Animal):
        self.animals[animal.gender].append(animal)

        return self

    def kill_off(self, animal: Animal):
        self.animals[animal.gender].remove(animal)
        self.deaths[animal.cause_of_death].append(animal)

        return self

    def breed(self):
        eligible_males = [male for male in self.animals[Gender.MALE] if male.can_breed()]
        eligible_females = [female for female in self.animals[Gender.FEMALE] if female.can_breed()]

        for male in eligible_males:
            if not eligible_females:
                break

            female = eligible_females.pop()

            # Check if there is enough food and water supply first.
            if female.can_eat() and female.can_drink():
                female.gestate()
                continue

            # If there wasn't enough food, then the eligible female will only have a small chance to breed.
            if random.choices((0, 1), (99.5, 0.5))[0]:
                female.gestate()

        return self

    def gestate(self):
        gestating_females = [female for female in self.animals[Gender.FEMALE] if female.is_gestating()]
        for gestating_female in gestating_females:
            gestating_female.gestate()

        return self

    def feed(self):
        animals = self.animals[Gender.FEMALE] + self.animals[Gender.MALE]

        while animals:
            animal = animals.pop(random.randint(0, len(animals) - 1))
            animal.drinks()

            if animal.is_dead():
                continue

            animal.eats()

        return self

    def habitate(self):
        for gender in self.animals:
            for animal in self.animals[gender]:
                animal.habitates().ages()

        return self

    def current_population_total(self) -> int:
        return len(self.animals[Gender.FEMALE]) + len(self.animals[Gender.MALE])

    def count_deaths_by_cause(self, cause: str) -> int:
        return len(self.deaths[cause])

    def total_deaths(self) -> int:
        total = 0

        for cause in self.deaths:
            total += len(self.deaths[cause])

        return total


class Animal:
    def __init__(self, species: Species, gender: str, habitat: Habitat):
        self.species = species
        self.gender = gender

        self.habitat = habitat
        self.age = 0
        self.starving_for = 0
        self.dehydrated_for = 0
        self.freezing_for = 0
        self.over_heated_for = 0
        self.gestating_for = -1
        self.cause_of_death = None

    def ages(self):
        self.age += 1

        if self.age == self.species.life_span:
            self.dies(CauseOfDeath.AGE)

        return self

    def starves(self):
        self.starving_for += 1

        if self.starving_for == self.species.max_survival_time_starvation:
            self.dies(CauseOfDeath.STARVATION)

        return self

    def goes_thirsty(self):
        self.dehydrated_for += 1

        if self.dehydrated_for == self.species.max_survival_time_dehydration:
            self.dies(CauseOfDeath.THIRST)

        return self

    def freezes(self):
        self.freezing_for += 1

        if self.freezing_for == self.species.max_survival_time_below_temp:
            self.dies(CauseOfDeath.COLD_WEATHER)

        return self

    def over_heats(self):
        self.over_heated_for += 1

        if self.over_heated_for == self.species.max_survival_time_above_temp:
            self.dies(CauseOfDeath.HOT_WEATHER)

        return self

    def eats(self) -> Animal:
        if self.can_eat():
            self.starving_for = 0
            self.habitat.remove_food(self.species.monthly_food_consumption)
        else:
            self.starves()

        return self

    def drinks(self) -> Animal:
        if self.can_drink():
            self.dehydrated_for = 0
            self.habitat.remove_water(self.species.monthly_water_consumption)
        else:
            self.goes_thirsty()

        return self

    def habitates(self) -> Animal:
        if self.habitat.temperature > self.species.maximum_temperature:
            self.over_heats()
        elif self.habitat.temperature < self.species.minimum_temperature:
            self.freezes()
        else:
            self.freezing_for = 0
            self.over_heated_for = 0

        return self

    def dies(self, cause_of_death: str):
        self.cause_of_death = cause_of_death
        self.species.kill_off(self)

    def can_breed(self) -> bool:
        return (self.species.minimum_breeding_age <= self.age <= self.species.maximum_breeding_age
                and self.gestating_for < 0)

    def can_eat(self) -> bool:
        return self.habitat.has_enough_food(self.species.monthly_food_consumption)

    def can_drink(self):
        return self.habitat.has_enough_water(self.species.monthly_water_consumption)

    def gestate(self) -> Animal:
        # @TODO should error out if this was a male...
        self.gestating_for += 1

        if self.gestating_for == self.species.gestation_period:
            self.give_birth()

        return self

    def give_birth(self) -> Animal:
        animal = Animal(species=self.species, gender=random.choice([Gender.FEMALE, Gender.MALE]),
                        habitat=self.habitat)
        self.gestating_for = -1
        self.species.add_animal(animal)

        return animal

    def is_gestating(self) -> bool:
        return self.gestating_for > -1

    def is_dead(self) -> bool:
        return self.cause_of_death is not None
