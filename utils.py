class ConfigValidator:
    SCHEMA = {
        'years': {'type': 'integer'},
        'iterations': {'type': 'integer'},
        'species': {
            'type': 'list',
            'schema': {
                'type': 'dict',
                'schema': {
                    'name': {'type': 'string'},
                    'attributes': {
                        'type': 'dict',
                        'schema': {
                            'monthly_food_consumption': {'type': 'integer'},
                            'monthly_water_consumption': {'type': 'integer'},
                            'life_span': {'type': 'integer'},
                            'minimum_breeding_age': {'type': 'integer'},
                            'maximum_breeding_age': {'type': 'integer'},
                            'gestation_period': {'type': 'integer'},
                            'minimum_temperature': {'type': 'integer'},
                            'maximum_temperature': {'type': 'integer'},
                        }
                    },
                }
            }

        },
        'habitats': {
            'type': 'list',
            'schema': {
                'type': 'dict',
                'schema': {
                    'name': {'type': 'string'},
                    'monthly_food': {'type': 'integer'},
                    'monthly_water': {'type': 'integer'},
                    'average_temperature': {
                        'type': 'dict',
                        'schema': {
                            'summer': {'type': 'integer'},
                            'spring': {'type': 'integer'},
                            'fall': {'type': 'integer'},
                            'winter': {'type': 'integer'},
                        }
                    },
                }
            }
        },
    }

    def __init__(self, engine):
        self._validator = engine()

    def validate(self, data: dict):
        self._validator.validate(data, ConfigValidator.SCHEMA)


class ConfigReader:
    def __init__(self, loader, file: str, validator: ConfigValidator):
        self._file = file
        self._loader = loader
        self._validator = validator

    def read(self) -> dict:
        with open(self._file) as stream:
            try:
                data = self._loader.safe_load(stream)
            except self._loader.YAMLError as err:
                data = err

        self._validator.validate(data)

        return data
