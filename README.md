
# Species Sim

## REQUIREMENTS
    - Python 3.7+

## SETUP

Create your virtual environment:

    $ virtualenv -p python3 simulator-env
    
    $ source simulator-env/bin/activate
     
    $ pip install -r requirements.txt
    
You can deactivate this by typing `deactivate`

If you don't have virtualenv installed:

    $ sudo pip install virtualenv

## TESTING

    $ python3 -m unittest test_*.py

## RUNNING

Activate the virtual env:

    $ source simulator-env/bin/activate
    
Run by passing the location of the config file:

    $ python3 simulator.py config.yaml
    
