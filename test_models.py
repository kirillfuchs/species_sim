from models import Animal, Habitat, Species
import yaml
import unittest
from simulator import SimulationFactory
from constants import Gender, CauseOfDeath

with open('test_mocks/test_config.yaml') as stream:
    data = yaml.safe_load(stream)

HABITAT_DATA = data['habitats'][0]
SPECIES_DATA = data['species'][0]


class TestHabitat(unittest.TestCase):
    def test_habitat_can_increment_month(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        hab.month = 1
        hab.increment_month()
        self.assertEqual(hab.month, 2)

    def test_habitat_does_not_increment_month_past_twelve(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        hab.month = 12
        hab.increment_month()
        self.assertEqual(hab.month, 1)

    def test_habitat_generates_food(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        start_supply = hab.food_supply
        hab.generate_food()
        self.assertEqual(hab.food_supply, start_supply + hab._monthly_food_regen)

    def test_habitat_generates_water(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        start_supply = hab.water_supply
        hab.generate_water()
        self.assertEqual(hab.water_supply, start_supply + hab._monthly_water_regen)

    def test_habitat_can_remove_food(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        start_supply = hab.food_supply
        hab.remove_food(10)
        self.assertEqual(hab.food_supply, start_supply - 10)

    def test_habitat_can_remove_water(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        start_supply = hab.water_supply
        hab.remove_water(10)
        self.assertEqual(hab.water_supply, start_supply - 10)

    def test_habitat_has_enough_food(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        self.assertTrue(hab.has_enough_food(0))

    def test_habitat_does_not_have_enough_food(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        self.assertFalse(hab.has_enough_food(100000))

    def test_habitat_has_enough_water(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        self.assertTrue(hab.has_enough_water(0))

    def test_habitat_does_not_have_enough_water(self):
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        self.assertFalse(hab.has_enough_water(100000))


class TestAnimals(unittest.TestCase):
    def test_animal_ages(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        start_age = male.age
        male.ages()
        self.assertEqual(male.age, start_age + 1)

    def test_animal_dies_after_too_old(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        [male.ages() for i in range(spe.life_span)]
        self.assertEqual(male.cause_of_death, CauseOfDeath.AGE)

    def test_animal_dies_after_starving(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        [male.starves() for i in range(spe.max_survival_time_starvation)]
        self.assertEqual(male.cause_of_death, CauseOfDeath.STARVATION)

    def test_animal_dies_after_dehydrating(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        [male.goes_thirsty() for i in range(spe.max_survival_time_dehydration)]
        self.assertEqual(male.cause_of_death, CauseOfDeath.THIRST)

    def test_animal_dies_after_freezing(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        [male.freezes() for i in range(spe.max_survival_time_below_temp)]
        self.assertEqual(male.cause_of_death, CauseOfDeath.COLD_WEATHER)

    def test_animal_dies_after_over_heating(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        [male.over_heats() for i in range(spe.max_survival_time_above_temp)]
        self.assertEqual(male.cause_of_death, CauseOfDeath.HOT_WEATHER)

    def test_animal_eats_and_resets_starvation_counter(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        hab.food_supply = 0
        self.assertEqual(male.starving_for, 0)
        male.eats()
        male.eats()
        self.assertEqual(male.starving_for, 2)
        hab.food_supply = spe.monthly_food_consumption
        male.eats()
        self.assertEqual(male.starving_for, 0)
        self.assertEqual(hab.food_supply, 0)

    def test_animal_eats_and_resets_thirst_counter(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(male)
        hab.water_supply = 0
        self.assertEqual(male.dehydrated_for, 0)
        male.drinks()
        male.drinks()
        self.assertEqual(male.dehydrated_for, 2)
        hab.water_supply = spe.monthly_water_consumption
        male.drinks()
        self.assertEqual(male.dehydrated_for, 0)
        self.assertEqual(hab.water_supply, 0)

    def test_animal_gives_birth(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA)
        female = Animal(spe, Gender.FEMALE, hab)
        spe.add_animal(female)
        female.give_birth()
        self.assertEqual(spe.current_population_total(), 2)


class TestSpecies(unittest.TestCase):
    def test_species_can_breed(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA).generate_water().generate_food()
        female = Animal(spe, Gender.FEMALE, hab)
        female.age = spe.minimum_breeding_age
        male = Animal(spe, Gender.MALE, hab)
        male.age = spe.minimum_breeding_age
        spe.add_animal(female).add_animal(male)
        self.assertTrue(female.can_breed())

        spe.breed()
        self.assertTrue(female.is_gestating())

    def test_species_can_feed(self):
        spe = SimulationFactory().start_species(SPECIES_DATA)
        hab = SimulationFactory().start_habitat(HABITAT_DATA).generate_water().generate_food()
        female = Animal(spe, Gender.FEMALE, hab)
        male = Animal(spe, Gender.MALE, hab)
        spe.add_animal(female).add_animal(male)
        start_food_supply = hab.food_supply
        start_water_supply = hab.water_supply

        spe.feed()
        self.assertEqual(hab.food_supply,
                         start_food_supply - (spe.monthly_food_consumption * spe.current_population_total()))
        self.assertEqual(hab.water_supply,
                         start_water_supply - (spe.monthly_water_consumption * spe.current_population_total()))
