class Gender:
    MALE = 'male'
    FEMALE = 'female'


class CauseOfDeath:
    STARVATION = 'starvation'
    THIRST = 'thirst'
    AGE = 'age'
    COLD_WEATHER = 'cold_weather'
    HOT_WEATHER = 'hot_weather'
