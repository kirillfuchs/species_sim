from utils import ConfigReader
import yaml
from unittest.mock import Mock
import unittest


class TestConfigReader(unittest.TestCase):
    def test_configreader_can_read(self):
        validator = Mock()
        data = ConfigReader(yaml, './test_mocks/test_config.yaml', validator).read()
        self.assertIsInstance(data, dict)
        validator.validate.assert_called()
